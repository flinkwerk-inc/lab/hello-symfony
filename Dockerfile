# This Dockerfile uses the latest version of the Bitnami PHP-FPM Docker image
FROM bitnami/php-fpm:latest

# Copy app's source code to the /app directory
COPY assets/ ./assets
COPY bin/ ./bin
COPY config/ ./config
COPY migrations/ ./migrations
COPY public/ ./public
COPY src/ ./src
COPY templates/ ./templates
COPY translations/ ./translations
COPY .env ./
COPY composer.json ./
COPY composer.lock ./
COPY symfony.lock ./
COPY webpack.config.js ./

# The application's directory will be the working directory
WORKDIR /app
