<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Pizza;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class HealthCheckAction extends AbstractController
{

    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    #[Route('/list', name: 'list_pizzas')]
    public function __invoke(): Response
    {
        $pizzaRepository = $this->entityManager->getRepository(Pizza::class);

        $pizza = new Pizza();
        $pizza->setName(sprintf('Pizza-%s', rand(0, 1000)));

//        $this->entityManager->persist($pizza);
//        $this->entityManager->flush();

        $pizzas = $pizzaRepository->findAll();

        return $this->render('base.html.twig', ['pizzas' => $pizzas]);
    }
}
